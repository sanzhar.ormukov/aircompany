package planes;
import java.util.Objects;

public class PrivatePlane  extends Plane {

    private String companyName;
    public PrivatePlane(String model, int maxSpeed, int maxFlightDistance, int maxLoadCapacity, String companyName) {
        super(model, maxSpeed, maxFlightDistance, maxLoadCapacity);
        if (companyName != null){
            this.companyName = companyName;
        }
        else {
            throw new IllegalArgumentException("Company name can't be null");
        }
    }

    public String getCompanyName() {
        return companyName;
    }

    @Override
    public String toString() {
        return "Private" + super.toString().replace("}",
                ", company name=" + companyName +
                        '}');
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PrivatePlane){
            PrivatePlane privatePlane = (PrivatePlane) obj;
            return super.equals(obj) && companyName.equals(privatePlane.companyName);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), companyName);
    }
}
