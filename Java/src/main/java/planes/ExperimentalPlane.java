package planes;
import models.ClassificationLevel;
import models.ExperimentalTypes;
import java.util.Objects;

public class ExperimentalPlane extends Plane{

    private ExperimentalTypes type;
    private ClassificationLevel classificationLevel;

    public ExperimentalPlane(String model, int maxSpeed, int maxFlightDistance, int maxLoadCapacity, ExperimentalTypes type, ClassificationLevel classificationLevel) {
        super(model, maxSpeed, maxFlightDistance, maxLoadCapacity);
        this.type = type;
        this.classificationLevel = classificationLevel;
    }

    public ExperimentalTypes getType() {
        return type;
    }

    public ClassificationLevel getClassificationLevel(){
        return classificationLevel;
    }

    public void setClassificationLevel(ClassificationLevel classificationLevel){
        this.classificationLevel = classificationLevel;
    }

    @Override
    public String toString() {
        return "Experimental" + super.toString().replace("}",", type='" + type + '\'' +
                ", classificationLevel='" + getClassificationLevel() + '\'' +
                '}');
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ExperimentalPlane){
            ExperimentalPlane experimentalPlane = (ExperimentalPlane) obj;
            return super.equals(obj) && type == experimentalPlane.type &&
                    classificationLevel == experimentalPlane.classificationLevel;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type, classificationLevel);
    }
}
